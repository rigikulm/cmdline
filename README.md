# cmdline
Sample program that accepts various commnd line options and
prints the selections to standard out.

## Usage
The current version also displays back the list of words typed into stdin
or piped in from a file

```bash
$ cmdline -h
$
$ cmdline -sha256 -v
  sha256: true
  v:      true
$
$ cmdline -v < README.md
```
