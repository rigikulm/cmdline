// cmdline - accepts various command line options and prints them
//
// Changelog:
// - Adding support for command line flags and accepting input from stdin
//
package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

func main() {
	var words map[string]int = make(map[string]int)
	sha256Flag := flag.Bool("sha256", false, "generate a SHA-256 hash")
	verboseFlag := flag.Bool("v", false, "display verbose (detailed) results")
	fmt.Println("Welcome to cmdline")
	flag.Parse()
	fmt.Println("sha256: ", *sha256Flag)
	fmt.Println("v: ", *verboseFlag)

	// Now after processing flags - lets see what's left of stdin
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		val := scanner.Text()
		_, ok := words[val]
		if !ok {
			words[val] = 1
		} else {
			words[val]++
		}
	}

	for key, val := range words {
		fmt.Printf("%s: \t %d\n", key, val)
	}

}
